#!/usr/bin/python
# -*- coding: utf-8 -*-
#
import getpass
import os
import sys
import telnetlib
import time

user = raw_input("Enter your AD Username: ")
password = getpass.getpass()

fo = open("ipfile.txt", "r")
Lines = fo.readlines()

outputFile = "/home/x1viveth/Python_Scripts/Output/output_AP_Tacacs.txt"
fo_switch = open(outputFile, "w+")

for line in Lines:
	Host = line.strip()
	print "Configuring Switch " + Host
	try:
    	tn = telnetlib.Telnet(Host)
    	tn.read_until("Username: ")
    	tn.write(user + "\n")
    	tn.read_until("Password: ")
    	tn.write(password + "\n")
    	tn.write("sh run | sec tacacs\n")
    	tn.write("exit\n")
    	#print tn.read_all()
    	output = tn.read_all()
    	fo_switch.write(output)
    except Exception as msg:
        print "Error while login to {}\nError: {}".format(line, msg)
fo_switch.close()